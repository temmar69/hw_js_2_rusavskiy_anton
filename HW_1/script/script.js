var x = 6;
var y = 14;
var z = 4; 

console.log('x = '+ x  + '; y = ' + y + '; z = '+ z  );
document.write(`x = ${x};<br> y = ${y};<br> z = ${z}`);

x += y - x++ * z; 
//  x = x + y - (x*z); x++ не инкрементируется в данном уравнении, так как инкремент происходить после действия;
//  x = 6 + 14  - 24 = -4 
console.log( x );
console.log('x = '+ x  + '; y = ' + y + '; z = '+ z  );
document.write(`<br><br><br> x = ${x};<br> y = ${y};<br> z = ${z}`);


x = 6;
y = 14;
z = 4; 
z = --x - y * 5;
// z = (x-1) - (y*5);
// z = 5 - 70 = -65;
console.log( z );
console.log('x = '+ x  + '; y = ' + y + '; z = '+ z  );
document.write(`<br><br><br> x = ${x};<br> y = ${y};<br> z = ${z}`);


x = 6;
y = 14;
z = 4; 
y /= x + 5 % z;
// y = y/(x + 5%z);
// y = 14/(6 + 1)=2;
console.log( y );
console.log('x = '+ x  + '; y = ' + y + '; z = '+ z  );
document.write(`<br><br><br> x = ${x};<br> y = ${y};<br> z = ${z}`);


x = 6;
y = 14;
z = 4;
var result1 = z - x++ + y * 5;
//result1 = z - x + (y*5);
//result1 = 4 - 6 + 70 = 68;
console.log(result1);
console.log('x = '+ x  + '; y = ' + y + '; z = '+ z + '; result1 = ' + result1  );
document.write(`<br><br><br> z - x++ + y * 5 = ${result1};  <br>x = ${x};<br> y = ${y};<br> z = ${z}`);


x = 6;
y = 14;
z = 4;
x = y - x++ * z;
// x = y - (x*z);
// x = 14 - (6*4) = 14 - 24 = -10;
console.log( x );
console.log('x = '+ x  + '; y = ' + y + '; z = '+ z  );
document.write(`<br><br><br> x = ${x};<br> y = ${y};<br> z = ${z}`);
